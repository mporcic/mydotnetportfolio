import { PageinfoComponent } from './components/pageinfo/pageinfo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { TooltipModule } from "ng2-tooltip";

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { IconsService } from './services/icons.service';
import { ProjectsComponent } from './components/projects/projects.component';
import { ProjectService } from './services/project.service';
import { ProjectFormComponent } from './components/project-form/project-form.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        ProjectFormComponent,
        ProjectsComponent,
        ContactComponent,
        PageinfoComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        TooltipModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'projects', component: ProjectsComponent },
            { path: 'contact', component: ContactComponent },
            { path: 'about', component: PageinfoComponent },
            { path: 'projects/new', component: ProjectFormComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
        ProjectService,
        IconsService
    ]

})
export class AppModuleShared {
}
