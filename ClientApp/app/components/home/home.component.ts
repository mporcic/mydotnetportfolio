import { IconsService } from './../../services/icons.service';
import { Component } from '@angular/core';
import axios from 'axios';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    technologies: any[] = [];
    constructor(private iconService:IconsService) {}
    ngOnInit() {
        axios.get('/api/technologies')
      .then(data => { this.technologies = data.data;})
      .catch(error=>console.error(error.response.data));
    }
}
