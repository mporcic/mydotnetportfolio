import { ProjectService } from './../../services/project.service';
import { Component, OnInit } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {

  frontendTechnologies: any[] = [];
  backendTechnologies: any[] = [] ;
  databaseTechnologies: any[] = [];
  project:any = {};
  
  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    axios.get('/api/technologies')
    .then(data =>  { 
      let technologies:any[] = data.data;
      this.frontendTechnologies = technologies.filter(tc=>tc.type =='frontend');
      this.backendTechnologies = technologies.filter(tc=>tc.type == 'backend');
      this.databaseTechnologies = technologies.filter(tc=>tc.type == 'database');
   })
    .catch(error => console.error(error.response.data));
  }

  addProject() {
    axios.post('/api/projects', this.project).then(data=>console.log(data.data)).catch(error=>console.error(error.response.data));
  }

}
