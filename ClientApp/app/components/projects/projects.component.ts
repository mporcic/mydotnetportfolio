import { IconsService } from './../../services/icons.service';
import { Component, OnInit } from '@angular/core';
import {TooltipModule} from "ng2-tooltip";
import axios from 'axios';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: any[] = [];

  constructor(private iconService: IconsService) {}

  ngOnInit() {
    axios.get('/api/projects')
      .then(data => { this.projects = data.data;})
      .catch(error=>console.error(error.response.data));
  }
}
