import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Injectable()
export class IconsService {

  constructor(private sanitizer: DomSanitizer) { }

  getLogo(technologyName:string, project: boolean){
    switch(technologyName){
      case 'PHP':
        return "<i class='devicon-php-plain colored'></i>";
      case 'ReactJS':
          return project ? "<i class='devicon-react-original colored'></i> <i class='devicon-typescript-plain colored'></i>" : "<i class='devicon-react-original colored'></i>";
      case 'AngularJS':
          return project ?
              `<i class='devicon-angularjs-plain colored'></i>
                <i class='devicon-typescript-plain colored'></i>
              ` : "<i class='devicon-angularjs-plain colored'></i>" ;
      case 'VueJS':
        return "<i class='devicon-vuejs-plain colored'> </i>";
      case 'Python':
        return "<i class='devicon-python-plain colored'></i>";
      case 'Django':
        return "<i class='devicon-django-plain colored'></i>";
      case 'Laravel':
        return "<i class='devicon-laravel-plain colored'></i>";
      case '.NET':
        return "<i class='devicon-dot-net-plain colored'></i>";
      case 'iOS':
        return "<i class='devicon-apple-original colored'></i>";
      case 'Android':
        return "<i class='devicon-android-plain colored'></i>";
      case 'TypeScript':
        return "<i class='devicon-typescript-plain colored'></i>";
      case 'CSharp':
        return "<i class='devicon-csharp-plain colored'></i>";
      case 'MySQL':
        return "<i class='devicon-mysql-plain colored'></i>";
      case 'PostgreSQL':
        return "<i class='devicon-postgresql-plain colored'></i>";
      case 'SQL':
        return "<img src='/img/sql_logo.png' class='img-responsive sql-logo img-logo' />"
      case 'SQLite':
        return "<img src='/img/sqlite-logo.png' class='img-responsive horizontal-logo' />";
      case 'ElasticSearch':
        return "<img src='/img/elasticsearch-logo.svg' class='img-responsive sql-logo img-logo' />"
      case 'React-Native':
        return '<img src="/img/react-native-logo.png" alt="ASP.NET" class="img-responsive horizontal-logo" />'
      case 'Electron':
        return '<img src="/img/electron_logo.svg" alt="ASP.NET" class="img-responsive img-logo" />';
      case 'Java':
        return "<i class='devicon-java-plain colored'></i>";
      case 'HTML/CSS/JavaScript':
        return `
          <i class="devicon-html5-plain colored"></i>
          <i class="devicon-css3-plain colored"></i>
          <i class="devicon-javascript-plain colored"></i>
        `
      case 'Bash':
        return "<img src='/img/bash-logo.png' class='img-responsive bash-logo' alt='Bash' />"
      case 'Prometheus':
        return "<img src='/img/prometheus-logo.png' class='img-responsive img-logo' alt='Prometheus' />"
      case 'Dart':
        return "<img src='/img/dart_logo.png' class='img-responsive img-logo' alt='Dart' />"
      case 'Android and iOS':
        return project ? `
          <i class='devicon-apple-original colored'></i>
          <i class='devicon-android-plain colored'></i>
            ` : '';
    }
  }

}
