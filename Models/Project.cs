using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dotnetportfolio.Models
{
    [Table("Projects")]
    public class Project
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        public int FrontendTechnologyId { get; set; }
        public Technology FrontendTechnology { get; set; }
        public int BackendTechnologyId { get; set; }
        public Technology BackendTechnology { get; set; }
        public int DatabaseTechnologyId { get; set; }
        public Technology DatabaseTechnology { get; set; }
    }
}