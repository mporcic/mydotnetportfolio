﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace mydotnetportfolio.Migrations
{
    public partial class DatabaseSeeder : Migration
    {
         public List<String> FrontendTechnologies = new List<String>() {"None", "HTML/CSS/JavaScript", "Android", "iOS", "ReactJS", "AngularJS", "VueJS", "TypeScript", "Electron", "React-Native", "Android and iOS"};

        public List<String> BackendTechnologies = new List<String>() {"None", ".NET", "PHP", "Laravel", "Python", "Django", "Java", "GoLang", "CSharp", "NodeJS"};

        public List<String> DatabaseTechnologies = new List<String>() {"None", "SQL", "MySQL", "SQLite", "PostrgreSQL", "ElasticSearch", "MongoDB"};

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            foreach(string technology in FrontendTechnologies) {
                migrationBuilder.Sql($"INSERT INTO Technologies (Name, Type) VALUES ('{technology}', 'frontend')");
            }

            foreach(string technology in BackendTechnologies) {
                migrationBuilder.Sql($"INSERT INTO Technologies (Name, Type) VALUES ('{technology}', 'backend')");
            }

            foreach(string technology in DatabaseTechnologies) {
                migrationBuilder.Sql($"INSERT INTO Technologies (Name, Type) VALUES ('{technology}', 'database')");
            }
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
