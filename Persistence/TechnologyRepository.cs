using System.Collections.Generic;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;

namespace mydotnetportfolio.Persistence
{
    public class TechnologyRepository : ITechnologyRepository
    {
        private readonly PortfolioContext context;

        public TechnologyRepository(PortfolioContext context)
        {
            this.context = context;
        }

        public IEnumerable<Technology> getTechnologies()
        {
            return context.Technologies;
        }
    }
}