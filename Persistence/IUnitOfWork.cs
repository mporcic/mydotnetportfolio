using System.Threading.Tasks;

namespace mydotnetportfolio.Persistence
{
    public interface IUnitOfWork
    {
         Task CompleteAsync();
    }
}