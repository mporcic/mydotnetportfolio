using System.Collections.Generic;
using System.Threading.Tasks;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;
using Microsoft.EntityFrameworkCore;

namespace mydotnetportfolio.Persistence
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly PortfolioContext context;

        public ProjectRepository(PortfolioContext context) {
            this.context = context;
        }

        public async Task<IEnumerable<Project>> getProjects() {
            return await context.Projects.Include(p=>p.FrontendTechnology)
                .Include(p=>p.BackendTechnology)
                .Include(p=>p.DatabaseTechnology)
                .ToListAsync();
        }

        public void Add(Project project) {
            context.Projects.Add(project);
        }
    }
}