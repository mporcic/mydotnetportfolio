using System.Threading.Tasks;
using dotnetportfolio.Persistence;

namespace mydotnetportfolio.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PortfolioContext context;

        public UnitOfWork(PortfolioContext context)
        {
            this.context = context;
        }

        public async Task CompleteAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}