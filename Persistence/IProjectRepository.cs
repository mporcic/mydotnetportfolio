using System.Collections.Generic;
using System.Threading.Tasks;
using dotnetportfolio.Models;

namespace mydotnetportfolio.Persistence
{
    public interface IProjectRepository
    {
         Task<IEnumerable<Project>> getProjects();
         void Add(Project project);
    }
}