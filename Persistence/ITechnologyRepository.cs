using System.Collections.Generic;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;

namespace mydotnetportfolio.Persistence
{
    public interface ITechnologyRepository
    {
        IEnumerable<Technology> getTechnologies();
    }
}