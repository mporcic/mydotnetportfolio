using dotnetportfolio.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnetportfolio.Persistence
{
    public class PortfolioContext : DbContext
    {
       public DbSet<Project> Projects { get; set; }
       public DbSet<Technology> Technologies { get; set; }
        
        public PortfolioContext(DbContextOptions<PortfolioContext> options) : base(options)
        {
            
        }

    }
}