# Welcome to my .Net Portfolio!
I have written this portfolio with the purpose of demonstrating my skills in creating applications with .Net and AngularJS.

Besides AngularJS I am able to recreate this in ReactJS and VueJS as well without a problem.

## This project was built with:
* [ASP.NET Core]("https://www.microsoft.com/net") and [C#]("https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/index") for cross-platform server-side code

* [AngularJS]("https://angular.io") and [TypeScript]("http://www.typescriptlang.org") for client-side code

* [Webpack]("https://webpack.github.io") for building and bundling client-side resources

* [Bootstrap]("http://getbootstrap.com") for layout

* It is being hosted on my [Raspberry Pi]("https://www.raspberrypi.org")

## Aditional notes about this project
It uses an SQLite database for easy deployment purposes (because I am running this live on an ARM processor)

I've implemented a few design patterns (UnitOfWork, Repository, Automapping resources) that might be considered overkill for such a simple project, but I've wanted to demonstrate my knowledge of them.

Now, the application is loosely coupled and ready for UnitTest and Agile development and scaling.

You can find a live representation by visiting http://redicode.me