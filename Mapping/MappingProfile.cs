using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using dotnetportfolio.Controllers.Resources;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;

namespace angular.Mapping
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            //resources to domain classes
            CreateMap<ProjectResource, Project>();

            //domain classes to resources
            CreateMap<Project, ProjectResource>();
        }
    }
}