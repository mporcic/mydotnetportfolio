using System.Collections.Generic;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace dotnetportfolio.Controllers
{
    public class TechnologiesController : Controller
    {
        private readonly PortfolioContext context;

        public TechnologiesController(PortfolioContext context) {
            this.context = context;
        }

        [HttpGet("/api/technologies")]
        public IEnumerable<Technology> GetTechnologies() {
            return context.Technologies;
        }

    }
}