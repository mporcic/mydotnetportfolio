using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using dotnetportfolio.Controllers.Resources;
using dotnetportfolio.Models;
using dotnetportfolio.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mydotnetportfolio.Persistence;

namespace dotnetportfolio.Controllers
{
    public class ProjectsController:Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IProjectRepository repository;

        public ProjectsController(IUnitOfWork unitOfWork, IMapper mapper, IProjectRepository repository) {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.repository = repository;
        }

        [HttpGet("/api/projects")]
        public async Task<IEnumerable<ProjectResource>> GetProjects() {
            var projects = await repository.getProjects();
            var result = mapper.Map<IEnumerable<Project>, IEnumerable<ProjectResource>>(projects);
            return result;
        }

        [HttpPost("/api/projects")]
        public async Task<IActionResult> CreateProject([FromBody]Project project) {
            repository.Add(project);
            await unitOfWork.CompleteAsync();       
            return Ok(project);
        }
    }
}