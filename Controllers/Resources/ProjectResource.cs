using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using dotnetportfolio.Models;

namespace dotnetportfolio.Controllers.Resources
{
    public class ProjectResource
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
        public Technology FrontendTechnology { get; set; }
        public Technology BackendTechnology { get; set; }
        public Technology DatabaseTechnology { get; set; }
    }
}